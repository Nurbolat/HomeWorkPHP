<?php
return [
    'Hello'=>'Hello there.',
    'Name'=>'Name',
    'Surname'=>'Surname',
    'Salary'=>'Salary',
    'Position'=>'Position',
    'Actions'=>'Actions',
    'Image'=>'Image'
];