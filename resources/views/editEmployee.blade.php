@extends('layouts.app');
@section('content')
    <div class="container">
        <div class="col-sm-offset-2 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    New Employee
                </div>

                <div class="panel-body">
                    @if(session()->get("status"))
                        <div class="alert alert-success">
                            {{session()->get('status')}}
                        </div>
                    @endif
                    <form action="/employees/{{$emp->id}}" method="POST" class="form-horizontal">
                        {{ csrf_field() }}
                        {{ method_field('PUT')}}
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Name</label>

                            <div class="col-sm-6">
                                <input type="text" name="name" id="task-name" class="form-control" value="{{ $emp->name }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Surname</label>

                            <div class="col-sm-6">
                                <input type="text" name="surname" id="task-name" class="form-control" value="{{ $emp->surname }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Salary</label>

                            <div class="col-sm-6">
                                <input type="text" name="salary" id="task-name" class="form-control" value="{{$emp->salary }}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="task-name" class="col-sm-3 control-label">Position</label>

                            <div class="col-sm-6">
                                <input type="text" name="position" id="task-name" class="form-control" value="{{ $emp->position }}">
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Save
                                </button>
                            </div>
                        </div>

                        {{--<form action="{{ url('employees/'.$task->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}

                            <button type="submit" class="btn btn-danger">
                                Delete
                            </button>
                        </form>--}}
                       {{-- <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-default">
                                    <i class="fa fa-btn fa-plus"></i>Add Employee
                                </button>
                            </div>
                        </div>--}}
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection


{{--
<div class="panel panel-default">
    <div class="panel-heading">
        Current Tasks
    </div>
    <div class="panel-body">
        <table class="table table-striped task-table">
            <thead>
            <th>Task</th>
            <th>&nbsp;</th>
            </thead>
            <tbody>
            @foreach($tasks as $task)
                <tr><td>{{$task->name}}</td>
                    <td>
                        <form action="{{ url('task/'.$task->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" class="btn btn-danger">
                                Delete
                            </button>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>--}}
