@extends('layouts.app')
@section('content')

    <div class="container">
        <h1 class="text-center">Latest news</h1>

        @foreach($news as $news2)
            <div class="news">
                <img src="{{$news2->image}}" alt="">
                <h2 class="text-center">News title</h2>
                <p>{{$news2->text}}</p>
                <div class="author">{{$news2->user->email}} </div>
                <div class="pDate">{{$news2->created_at}}</div>

                <h3 class="text-center">Comments</h3>
                <div class="comments">
                    <div class="comment text-center">
                        <div class="text">
                        @foreach($news2->comments as $comment)
                            <p>{{$comment->comment}}</p>
                                <h3 class="text-center">{{$comment->user->name}}</h3>
                            @endforeach
                    </div>
                    <form action="/comment" class="" method="post">
                        {{csrf_field()}}
                        <textarea cols="120" rows="10" class="text-center" name = "comment"></textarea>
                        <input type="hidden" value="{{$news2->id}}" name="news_id">
                        <br/>
                        <input type="submit" value="Отправить">
                    </form>
                </div>

            </div>
        @endforeach
                {{$news->links()}}
    </div>

@endsection

<style>
    .news{
        background: #eee;
        height: auto;
        padding: 20px;
        border-radius: 50px;
        margin-bottom: 10px;
    }
    .news h2{
        font-weight: bold;
        font-style: italic;
        margin: 0px;
    }
    .news p{
        text-align: justify;;
    }
    .news img{
        float: left;
        height: 180px;
        margin: 40px;
    }

    .author{
        float: right;
        color: #000;
    }

    .comments{
        margin: 50px 0px;
    }

    .cometee{
        float: right;
    }
</style>