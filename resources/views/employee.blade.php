@extends('layouts.app')
@section('content')
    <h1 class="text-center">{{$emp->name . "\t". $emp->surname}} </h1>
    <h2 class="text-center">{{"Salary \t".$emp->salary}}</h2>
    <h2 class="text-center">{{"Position \t".$emp->position}}</h2>
    <a href="/employees" class="btn btn-success">Back</a>
@endsection