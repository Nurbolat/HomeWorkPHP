@extends('layouts.app');
@section('content')
    <div class="container">
        <div class="panel panel-default">
            @if(session()->get("status"))
                <div class="alert alert-success">
                    {{session()->get('status')}}
                </div>
            @endif
            <div class="panel-heading">
                Current Employees
            </div>
            <div class="panel-body">
                <table class="table table-striped task-table">
                    <thead>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Salary</th>
                    <th>Position</th>
                    <th>Deleted_at</th>
                    </thead>
                    <tbody>
                    @foreach($fireds as $fired)
                        <tr>
                            <td>{{$fired->name}}</td>
                            <td>{{$fired->surname}}</td>
                            <td>{{$fired->salary}}</td>
                            <td>{{$fired->position}}</td>
                            <td>{{$fired->deleted_at}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <a href="/employees" class="btn btn-success">Back</a>
    </div>
@endsection