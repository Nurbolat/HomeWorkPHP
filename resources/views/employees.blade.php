@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="panel panel-default">
            @if(session()->get("status"))
                <div class="alert alert-success">
                    {{session()->get('status')}}
                </div>
            @endif
            <div class="panel-heading">
                Current Employees
            </div>
                <form action="/local" method="get">
                    <div>
                        <input type="submit" value="en" >
                        <input type="submit" value="ru">
                        <input type="submit" value="kz">
                    </div>
                </form>
            <div class="panel-body">
                <table class="table table-striped task-table">
                    <thead>
                    <th>
                        {{@trans("messages.Image")}}
                    </th>
                    <th>
                        {{@trans("messages.Name")}}
                    </th>
                    <th>
                        {{@trans("messages.Surname")}}
                    </th>
                    <th>
                        {{@trans("messages.Salary")}}
                    </th>
                    <th>
                        {{@trans("messages.Position")}}
                    </th>
                    <th>
                        {{@trans("messages.Actions")}}
                    </th>
                    <th>
                        &nbsp;</th>
                    </thead>
                    <tbody>
                    @foreach($employees as $employee)
                        <tr>
                            <td><img src="{{$employee->image}}" alt="" height="40px" width="40px"></td>
                            <td>{{$employee->name}}</td>
                            <td>{{$employee->surname}}</td>
                            <td>{{$employee->salary}}</td>
                            <td>{{$employee->position}}</td>
                            <td>
                                <form class="deleteform" action="{{ url('employees/'.$employee->id) }}" method="POST">
                                    {{ csrf_field() }}
                                    {{ method_field('DELETE') }}

                                    <button type="submit"  class="btn btn-danger">
                                        Delete
                                    </button>
                                    <a href="employees/{{$employee->id}}" class="btn btn-info">More</a>
                                    <a href="employees/{{$employee->id}}/edit" class="btn btn-warning">Edit</a>
                                </form>

                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <a href="/employees/create" class="btn btn-success">Add employees</a>
        <a href="/fired" class="btn btn-success">Show Fired</a>

    </div>
  @section("scripts")
      <script>
          $(function(){
              $('.deleteform').submit(function(e){
                 return false;
              });
          })
      </script>
      @endsection
@endsection