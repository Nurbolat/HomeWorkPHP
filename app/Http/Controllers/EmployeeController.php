<?php

namespace App\Http\Controllers;

use App\Employee;
use Illuminate\Http\Request;
use Illuminate\View\View;
use App;
use Storage;
class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request)
    {
        if($request->has("lang")){
            $request->lang;
            $this->validate($request,[
                "lang"=>in_array(["en","ru","kz"])
            ]);
            if($request->lang=="kz"){
                App::setlocale("kz");
            }
            else if($request->lang==ru){
                App::setlocale("ru");
            }
            else{
                App::setlocale("en");
            }

            //return App::getLocal();
        }
        $employees = Employee::all();
        return View("employees")->with(compact("employees"));
    }


    public function showFired()
    {
        $fireds = Employee::onlyTrashed()->get();
        return View("/fired")->with(compact("fireds"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("createEmployee")->with("status","Employee has been created");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           "salary"=>"required|integer|min:40000",
            "name"=>"string|required|max:255",
            "surname"=>"string|required|max:255",
            "position"=>"string|required|max:255"
        ]);
        $name = $request->name;
        $surname = $request->surname;
        $salary = $request->salary;
        $position = $request->position;
        $employee = new Employee();
        //   $employee = new Employee($request->all());
        $employee ->surname = $surname;
        $employee ->name = $name;
        $employee ->salary = $salary;
        $employee ->position = $position;
        $name2 = Storage::put("images",$request->file("image"));
        $url =Storage::url($name2);
        $employee->image = $url;
        $employee->save();
        return redirect("/employees");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $emp = Employee::find($id);
        return View("/employee")->with(compact("emp"));
    }

    public function home()
    {
       return view("home");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $emp=Employee::find($id);
        return view("/editEmployee")->with(compact("emp"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $emp = Employee::find($id);
        $copy = Employee::find($id);

        $emp->name = $request->name;
        $emp->surname = $request->surname;
        $emp->salary = $request->salary;
        $emp->position = $request->position ;
        $emp->save();
        return redirect("/employees")->with("status","Employee $copy->name $copy->surname change");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        Employee::destroy($id);
        return redirect("/employees")->with("status","Delete employee $employee->name $employee->surname");

    }

}
