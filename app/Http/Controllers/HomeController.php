<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Comment;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $news = News::with("user")->with("comments.user")->paginate(1);
        $news = News::with("user")->with("comments.user")->simplePaginate(1);//пагинация
        return view('home')->with(compact("news","coments"));
    }

    public function comment(Request $request)
    {
        $coments =new Comment();
        $coments->news_id=$request->news_id;
        $coments->comment=$request->comment;
        $coments->user_id=Auth::user()->id;
        $coments->save();
        return back();
    }
}
